import { Component } from '@angular/core';
import { stringify } from 'querystring';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  oddComponents = [];
  evenComponents = [];

  createComponent(number: number) {
    // console.log(number);
    if (number % 2 == 0) { // if number emitted is even, add a new component to the list and render it
      this.evenComponents.push(number);
    } else {
      this.oddComponents.push(number);
    }
  }
}

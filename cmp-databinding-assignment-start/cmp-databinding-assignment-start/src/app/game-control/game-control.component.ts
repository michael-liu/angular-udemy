import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  res = null;
  num = 0;
  @Output() gameStarted = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  startGame() {
    this.res = setInterval(() => {
      console.log(this.num)
      this.gameStarted.emit(
        this.num++
      );

    }, 1000);
  }

  stopGame() {
    clearInterval(this.res);
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.css']
})
export class AssignmentComponent implements OnInit {
  display = false;
  log = [];
  numItems = 0;

  constructor() { }

  ngOnInit() {
  }

  toggleButton(event: Event) {
    this.display = !this.display;
    this.log.push(event.timeStamp);
    console.log(event.timeStamp);
    this.numItems++;
  }
}

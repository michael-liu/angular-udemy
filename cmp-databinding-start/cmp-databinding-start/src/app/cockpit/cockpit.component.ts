import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { tokenName } from '@angular/compiler';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
  @Output() serverCreated = new EventEmitter<{
    serverName: string,
    serverContent: string
  }>();
  @Output() blueprintCreated = new EventEmitter<{
    serverName: string,
    serverContent: string
  }>();

  constructor() { }

  ngOnInit() {
  }

  onAddServer(name: HTMLInputElement, content: HTMLInputElement) {
    // this.serverElements.push({
    //   type: 'server',
    //   name: this.newServerName,
    //   content: this.newServerContent
    // });
    this.serverCreated.emit({
      serverName: name.value,
      serverContent: content.value
    });
  }

  onAddBlueprint(name: HTMLInputElement, content: HTMLInputElement) {
    // this.serverElements.push({
    //   type: 'blueprint',
    //   name: this.newServerName,
    //   content: this.newServerContent
    // });
    this.blueprintCreated.emit({
      serverName: name.value,
      serverContent: content.value
    });
  }
}

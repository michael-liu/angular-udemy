import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  username: ''

  usernameCheck() {
    if (this.username != null && this.username != '') {
      return false;
    } else {
      return true;
    }
  }

  resetUsername() {
    this.username = '';
  }
}

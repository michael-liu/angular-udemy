# TypeScript

### Imports
- typescript imports at the top of `app.module.ts`

- NgModule needs to have the import

### CSS
- include CSS files in `angular.json` under \<project> \> architect \> build \> styles

### App structure
- app is loaded via `.bootstrapModule(module_name_here)` in `main.ts`
    - module name usually refers to `app.module.ts` under `app`
- the module can continue to bootstrap other Angular components using the `bootstrap` field in `app.module.ts`

**NOTE: app related content goes under app folder; each component should have its own folder**
**naming convention: component-name.component**

### Components (a TypeScript class)
- `export class ComponentNameComponent { }` creates a normal TypeScript class; export makes this class usable outside of this file
- `@Component({ ... })` is the decorator for a component
    - all decorators start with @
    - metadata can be stored in the component
        - e.g.
        - `selector: 'app-component-name'` used as a HTML component
            - selectors can be attributes e.g. `[app-component-name]` means a `div` with the class `app-component-name` would be recognized as an `app-component-name` component
        - `templateUrl: './server.component.html'` used as the HTML template for the component; is a relative path to the HTML file
            - `template: <html code>` can be used similarly to ES6 inline templating; can be used instead of templateUrl
        - `styleUrls: ['./style1.css', './style2.css']` used to include multiple CSS files
            - `style: [CSS selectors]` ES6 inline styling, can be used instead of styleUrls
- `import { ComponentName } from '@angular/core';` used to import the `@Component` decorator
- `ng generate component <component-name>` / `ng g c <component-name>` used as a command to auto generate TS, HTML, and CSS
- `constructor() { }` is called when the component is created (similar to created()/mounted() in Vue)

### App module
- Angular doesn't scan components, it must be manually registered
- register in `app.module`
    ```typescript
    declarations: [
        ServerComponent
    ]
    ```
- and import in `app.module`

### Data binding
##### String interpolation (mainly used for output of data)
- `{{ var }}` or `{{ getVar() }}` used for string interpolation, only works when the variable between {{}} resolves (and converted) into a string
    - define var in the `export class ComponentNameComponent { }`
        - e.g.
            ```typescript
            export class ComponentNameComponent {
                var = "1";

                // a method defined that returns a string
                getVar() {
                    return this.var;
                }
            }
            ```
##### Property binding (similar to v-bind:class="")
- `<element [disabled]="booleanVar"></element>` square brackets indicates the property is bound to some variable

##### Event binding (similar to v-on="")
- `(event)="methodName()"` used to bind an event to execute a method when the event occurs
- `(event)="methodName('test', $event)"` $event used to pass event data
    ```typescript
    methodName(event: Event) {
        // this.value now holds the text in the input element
        this.value = (<HTMLInputElement>event.target).value // cast event to HTML element
    }
    ```

##### 2-way data binding (similar v-model)
- `[(ngModel)]="variableName"` updates variableName on input event
- still need to use event binding (not like v-model; only binds variable and nothing else)

##### Directives (similar to v-if/v-for/custom directive)
- e.g.
    ```typescript
    <a *ngIf='booleanValue; else otherwise'></a> // else indicates the element with marker otherwise will be displayed
    <ng-template #otherwise></ng-template> // #marker indicates a certain spot in the template that will be displayed conditionally
    ```
    - \* indicates this directive changes the structure of the DOM

    ```typescript
    <p [ngStyle]="{'backgroundColor': getColor()}"></p>
    ```
    - ngStyle has to be property bound, so adding square brackets binds a property to ngStyle
    ```typescript
    <p [ngClass]="{'online': serverStatus === 'online'}"></p>
    ```
    - ngClass to use dynamically bind a custom CSS class
    ```typescript
    <component *ngFor='let variable of arrayVariable; let i = index'></component>
    ```
    - *ngFor is similar to v-for; `variable` can be used as string interpolation
    - the index can also be retrieved from `ngFor` using `let var = index`

##### Advanced data binding and data passing
__Parent to child data flow; property binding on components__
```typescript
// parent component
<div [house]='object1'></div> // typically object1 will be set using ngFor

// child component
@Input() house: { field1, field2 }
// OR
@Input('name') house: { field1, field2 }
```
- can property bind `house` to a component to pass data from parent component to child component
- `@Input()` exposes this variable s.t. the parent component can see this variable
- if you pass in the optional argument, you must property bind the variable to `[name]` in the component

__Child to parent data flow; emitting data (similar to v-emit)__
```typescript
// parent component
@Output() serverCreated = new EventEmitter<{ field1: string, ... }>();
@Output('name') serverCreated = new EventEmitter<{ field1: string, ... }>();
...
onCreateServer() {
    // output variable must emit the object indicated
    this.serverCreated.emit({
        field1: this.field1,
        ...
    })
}

// child component
<p (serverCreated)="onServerAdded($event)"></p>
```
- parent creates event emitters and emits it
    - ensure the event emitters have the `@Output()` decorator so that they can be listened to
    - if optional argument is passed to `@Output('name')`, then child component must listen to that optional arg
- child component can listen to an emitted event e.g. serverCreated

__Local references__
```typescript
// parent component
<input #serverNameInput>
...
<button (click)="run(serverNameInput)"></button>
...
run(serverNameInput: HTML*Element) {
    ...
}
```
- usage: in cases where 2 way data is unnecessary
- local references can only be used in the template (not TS)

__ng-content (similar to slot)__
```typescript
// root
<app-my-component>
    <h1>hello</h1>
</app-my-component>

// my-component
<div>
    <p>stuff</p>
    <ng-content></ng-content>
</div>
```
- HTML content placed between a custom component can be slotted into the component between ng-content i.e. hello will appear under stuff

__@ViewChild (similar to $el)__
```typescript
@ViewChild('local-reference-name') serverContentInput: ElementRef;
// OR
@ViewChild(MyComponent) serverContentInput: ElementRef;
```
- gets access to DOM element that has a local reference

### Models
- create a model that specifies the structure of an object
```typescript
export class Box {
    constructor(public length: number, public width: number, public height: number) {}
}
```
- creates length, width and height instance variables and automaticallly assigns data from constructor to the instance variables

### Debugging
- sources > webpack > .

### View encapsulation
- Angular enforces CSS is applied on a per-component basis

### Lifecycle hooks
```typescript
ngOnChanges(changes: SmallChanges)

ngAfterViewInit() // similar to mounted(); can access DOM elements
```

### Instance variables
- instance variables in a component can be used within the HTML of that component
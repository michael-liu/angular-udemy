import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";

import { Recipe } from './recipe-book/recipe.model';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ShoppingItemComponent } from './shopping-list/shopping-item/shopping-item.component';
import { IngredientComponent } from './shopping-list/ingredient/ingredient.component';
import { RecipeItemComponent } from './recipe-book/recipe-item/recipe-item.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { EditShoppingListComponent } from './shopping-list/edit-shopping-list/edit-shopping-list.component';
import { RecipeBookComponent } from './recipe-book/recipe-book.component';
import { RecipeListComponent } from './recipe-book/recipe-list/recipe-list.component';
import { RecipeInstructionsComponent } from './recipe-book/recipe-instructions/recipe-instructions.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ShoppingItemComponent,
    IngredientComponent,
    RecipeItemComponent,
    ShoppingListComponent,
    EditShoppingListComponent,
    RecipeBookComponent,
    RecipeListComponent,
    RecipeInstructionsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

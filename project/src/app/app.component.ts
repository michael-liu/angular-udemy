import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'project';
  selection: String = '';

  select(string: string) {
    console.log('selection is ' + string);
    this.selection = string;
  }
}

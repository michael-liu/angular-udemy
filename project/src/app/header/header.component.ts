import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() selection = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  recipe() {
    console.log('recipe selected')
    this.selection.emit('recipe');
  }

  shopping() {
    console.log('shopping selected')
    this.selection.emit('shopping');
  }
}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Ingredient } from '../../shared/ingredient.model';

@Component({
  selector: 'app-edit-shopping-list',
  templateUrl: './edit-shopping-list.component.html',
  styleUrls: ['./edit-shopping-list.component.css']
})
export class EditShoppingListComponent implements OnInit {
  @Output() item = new EventEmitter<Ingredient>();
  constructor() { }

  ngOnInit() {
  }

  addShoppingListItem(name: HTMLInputElement, amount: HTMLInputElement) {
    this.item.emit(new Ingredient(name.value, parseInt(amount.value)));
  }
}
